/*Task3:
 	+ Increase the size of the first list from 10 000 elements to 100 000, and add then 10 000 instead of 1000
	+ Extract the middle part to a seperate function - the function should take the list of 10 000 integers as an argument and return the time in milliseconds
	+ Call the function 100 times to get a better calculation - store the 100 times in a separate list and calculate the average*/

import java.util.*;

public class MainLab4T3 {
	// method taking list as parameter and measuring the time
	public static int test(List<Integer> list) {
		Random generator = new Random();
		long startTime = System.currentTimeMillis();
		// add 10000 integers at the beginning of the list
		for (int i = 0; i < 10000; i++) {
			list.add(0, generator.nextInt(10) + 1);
		}
		// return measured time in milliseconds
		return (int) (System.currentTimeMillis() - startTime);
	}

	public static void main(String[] args) {
		Random generator = new Random();
		int a = generator.nextInt(10) + 1;
		System.out.println("Be patient! It's running...");

		// --------------ARRAYLIST PART---------------//
		List<Integer> time1 = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			List<Integer> list1 = new ArrayList<>();
			for (int j = 0; j < 100000; j++) {
				list1.add(a);
			}
			// store each time result in the list
			time1.add(test(list1));
		}
		// caculate average of 100 results
		int total1 = 0;
		for (int x : time1) {
			total1 += x;
		}
		double average1 = ((double)total1) / ((double)time1.size());

		System.out.println(average1);

		// --------------LINKEDLIST PART---------------//
		LinkedList<Integer> time2 = new LinkedList<>();
		for (int i = 0; i < 100; i++) {
			LinkedList<Integer> list2 = new LinkedList<>();
			for (int j = 0; j < 100000; j++) {
				list2.add(a);
			}
			// store each time result in the list 
			time2.add(test(list2));
		}
		// caculate average of 100 results
		int total2 = 0;
		for (int x : time2) {
			total2 += x;
		}
		double average2 = ((double)total2) / ((double)time2.size());
		
		System.out.println(average2);
	}
}
